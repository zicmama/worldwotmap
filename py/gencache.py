#!/usr/bin/env python3

import plyvel, json, urllib.request, networkx, random, math, re, utils
from math import *
from utils import *

CESIUMPLUS_URL = "https://g1.data.le-sou.org,https://g1.data.e-is.pro,https://g1.data.adn.life,https://g1.data.presles.fr"
DUNITER_PATH = "~/.config/duniter/duniter_default"
OUTPUT_PATH = "www/data"
OVERLAY_PRECISION = "0.5"

DATA_VERSION = 1
LAYOUT_ITERATIONS = 10
LAYOUT_GRAVITY = 0.1
LAYOUT_REPULSION = 0.00000005
LAYOUT_MINDIST = 0.0007
LAYOUT_MAXDIST = 0.0009
POS_ROUND = 7

def getRelationCircles(pubkey, maxLevel):
    circles = []
    circles[0] = [pubkey]

    for level in range(1, maxLevel+1):
        circles[level] = []
        for key in circles[level-1]:
            account = output["accounts"][key]
            for cert in account.certsIssued:
                if certs[cert].receiver not in circles[level]:
                    circles[level].append(certs[cert].receiver)
            for cert in account.certsReceived:
                if certs[cert].issuer not in circles[level]:  
                    circles[level].append(certs[cert].issuer)

    return circles

# Get the distance (m) between two points (lat,lon) (radians) on Earth (approximated to a sphere)
# https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates
def geodist(p1, p2):
	a = sin((p2[0]-p1[0])/2)**2 + sin((p2[1]-p1[1])/2)**2 * cos(p1[0]) * cos(p2[0])
	return 12742000*atan2(sqrt(a),sqrt(1-a))

MERCATOR_MAX_LAT = degrees(2*atan(e**pi)-pi/2)
def mercator(lat):
	return pi-math.log(tan(radians(lat)/2+pi/4))

def mercator_recip(lat):
	return degrees(2*atan(e**(pi-radians(lat)))-pi/2)

# TODO support multi-issuer
REGEX_SIG = re.compile("^SIG\\(([a-zA-Z0-9]+)\\)$")
def get_tx_result(tx):
	total = 0
	results = {}
	for i in tx["outputs"]:
		elems = i.split(":")
		try:
			pubkey = REGEX_SIG.finditer(elems[2]).__next__().groups()[0]
		except:
			continue
		amount = int(elems[0])*10**int(elems[1])
		total += amount
		if pubkey in results:
			results[pubkey] += amount
		else:
			results[pubkey] = amount
	
	issuer = tx["issuers"][0]
	if issuer in results:
		results[issuer] -= total
	else:
		results[issuer] = total
	
	return results

if __name__ == "__main__":
	if "--help" in sys.argv:
		print("""WorldWotMap data extractor (data v{})
CopyLeft 2018-2022 Pascal Engélibert (GNU AGPL v3)

Duniter must not be running while extracting LevelDB data.
Only compatible with Duniter >=1.7.9.

Options:
 -c <url>   Cesium+ URLs separated by commas (default={})
 -d <path>  Duniter profile path (default={})
 -e <path>  Output dir (default={})
 -o         Do not generate image overlays
 -op <nb>   Overlay precision (degrees) (default={})
 -r         Do not compute node repulsion
 --spam     Filter out some accounts considered as spam
 -v         Verbose mode
""".format(DATA_VERSION, CESIUMPLUS_URL, DUNITER_PATH, OUTPUT_PATH, OVERLAY_PRECISION))
		exit()
	
	if "-v" in sys.argv:
		utils.VERBOSITY |= LOG_TRACE
	
	antispam = "--spam" in sys.argv
	
	cesiumplus_urls = getargv("-c", CESIUMPLUS_URL).split(",")
	
	output_dir = format_path(getargv("-e", OUTPUT_PATH))
	try:
		os.mkdir(output_dir)
	except FileExistsError:
		pass
	
	output = {
		"version": DATA_VERSION,
		"certs": [],
		"accounts": {},
		"time": time.time(),
		"current_block": {
			"number": None,
			"hash": None,
			"median_time": None
		}
	}
	moredata = {}
	
	results = []
	query = {
		"query": {
			"bool": {
				"should": [
					{"exists": {"field": "geoPoint"}},
					{"bool": {"must": [
						{"exists": {"field": "title"}},
						{"exists": {"field": "uid"}}
					]}}
				]
			}
		},
		"size": 10000,
		"_source": ["title", "geoPoint", "avatar._content_type"]
	}
	
	for cesiumplus_url in cesiumplus_urls:
		log("Request Cesium+ {}".format(cesiumplus_url), LOG_TRACE)
		try:
			resp = json.loads(urllib.request.urlopen(
				cesiumplus_url+"/user/profile/_search?scroll=2m",
				json.dumps(query).encode()
			).read().decode())
		except urllib.error.HTTPError as err:
			log("Cesium+ HTTP Error: {}".format(err), LOG_ERROR)
			continue
		results += resp["hits"]["hits"]
		scroll_id = resp.get("_scroll_id")
		for i in range(100):
			log("Request Cesium+: scroll {}".format(i), LOG_TRACE)
			try:
				resp = json.loads(urllib.request.urlopen(
					cesiumplus_url+"/_search/scroll",
					json.dumps({
						"scroll": "1m",
						"scroll_id": scroll_id
					}).encode()
				).read().decode())
			except urllib.error.HTTPError as err:
				log("Cesium+ HTTP Error: {}".format(err), LOG_ERROR)
			results += resp["hits"]["hits"]
			scroll_id = resp.get("scroll_id")
			if not scroll_id:
				break
		break
	log("Cesium+ accounts: {}".format(len(results)), LOG_INFO)
	
	log("Opening DBs")
	duniter_path = format_path(getargv("-d", DUNITER_PATH))
	iindex = plyvel.DB(duniter_path + "data/leveldb/level_iindex")
	cindex = plyvel.DB(duniter_path + "data/leveldb/level_cindex")
	bindex = plyvel.DB(duniter_path + "data/leveldb/level_bindex")
	
	log("Fetching current block")
	b_iter = bindex.iterator()
	b_iter.seek_to_stop()
	current_block = json.loads(b_iter.prev()[1].decode())
	output["current_block"]["number"] = current_block["number"]
	output["current_block"]["hash"] = current_block["hash"]
	output["current_block"]["median_time"] = current_block["medianTime"]
	bindex.close()
	
	log("Iterating over identities")
	graph = networkx.Graph()
	account_index_by_id = []
	account_index_by_pk = {}
	neighbors = {}
	i = 0
	for pub, row in iindex:
		idty = json.loads(row.decode())[0]
		certs = json.loads(cindex.get(pub).decode())
		pubkey = idty["pub"]
		
		output["accounts"][pubkey] = [[idty["uid"], int(idty["member"]), None], None, None]
		
		if not pubkey in moredata:
			moredata[pubkey] = [0,0,0,0,0,None]
		
		if not pubkey in neighbors:
			neighbors[pubkey] = []
		
		for cert in certs["issued"]:
			if cert["expired_on"] == 0:
				output["certs"].append([pubkey, cert["receiver"]])
				if not cert["receiver"] in neighbors[pubkey]:
					neighbors[pubkey].append(cert["receiver"])
				if not cert["receiver"] in neighbors:
					neighbors[cert["receiver"]] = [pubkey]
				elif not pubkey in neighbors[cert["receiver"]]:
					neighbors[cert["receiver"]].append(pubkey)
				
				moredata[pubkey][1] += 1
				if cert["receiver"] in moredata:
					moredata[cert["receiver"]][0] += 1
				else:
					moredata[cert["receiver"]] = [1,0,0,0,0,None]
		
		account_index_by_id.append(pubkey)
		account_index_by_pk[pubkey] = i
		
		i += 1
	
	iindex.close()
	cindex.close()
	log("Identities: {}".format(len(output["accounts"])), LOG_INFO)
	log("Certifications: {}".format(len(output["certs"])), LOG_INFO)
	
	log("Computing communities")
	graph.add_nodes_from(range(0, len(output["accounts"])))
	for cert in output["certs"]:
		graph.add_edge(account_index_by_pk[cert[0]], account_index_by_pk[cert[1]])
	communities = [i for i in networkx.community.label_propagation_communities(graph)]
	
	log("Communities: {}".format(len(communities)), LOG_INFO)
	output["communities"] = len(communities)
	for community in range(len(communities)):
		for i in communities[community]:
			output["accounts"][account_index_by_id[i]][0][2] = community
	
	do_repulsion = not "-r" in sys.argv
	do_overlays = not "-o" in sys.argv
	if do_overlays:
		from PIL import Image
		windex = plyvel.DB(duniter_path + "data/leveldb/level_wallet")
		overlay_precision = float(getargv("-op", OVERLAY_PRECISION))
		overlay_size = ceil(360/overlay_precision)
	
	log("Iterating over Cesium+ accounts")
	overlay_account_density = {}
	overlay_money_density = {}
	for account in results:
		pubkey = account["_id"]
		geopoint = account["_source"].get("geoPoint")
		data = [
			account["_source"].get("title"),
			"avatar" in account["_source"],
			[
				round(float(geopoint["lat"]), POS_ROUND),
				round(float(geopoint["lon"]), POS_ROUND)
			] if geopoint else None
		]
		
		if antispam:
			if (
				data[1] # has avatar
				and not pubkey in output["accounts"] # not in WoT
				and geopoint # has position
				and data[2][0] >= 44.5058824 # is in the G area
				and data[2][0] <= 47.8
				and data[2][1] >= 0.8352941
				and data[2][1] <= 2.7176471 # is in the lattice:
				and (abs(47.8-data[2][0])%0.0235294 < 0.00001 or abs(47.8-data[2][0])%0.0235294 > 0.0235194)
				and (abs(1.4-data[2][1])%0.0235294 < 0.00001 or abs(1.4-data[2][1])%0.0235294 > 0.0235194)
			):
				continue
		
		if pubkey in moredata:
			moredata[pubkey][5] = data[2]
		else:
			moredata[pubkey] = [0,0,0,0,0,data[2]]
		
		if do_overlays and abs(data[2][0]) < MERCATOR_MAX_LAT:
			pos = (int((data[2][1]+180)/overlay_precision), int(mercator(data[2][0])/tau*overlay_size))
			
			if pos in overlay_account_density:
				overlay_account_density[pos] += 1
			else:
				overlay_account_density[pos] = 1
			
			wallet = windex.get(("SIG({})".format(pubkey)).encode())
			if wallet:
				wallet = json.loads(wallet)
				if pos in overlay_money_density:
					overlay_money_density[pos] += wallet["balance"]
				else:
					overlay_money_density[pos] = wallet["balance"]
				
				moredata[pubkey][2] = wallet["balance"]
		
		if pubkey in output["accounts"]:
			output["accounts"][pubkey][1] = data
		else:
			output["accounts"][pubkey] = [None, data, None]
	
	log("Total accounts: {}".format(len(output["accounts"])), LOG_INFO)
	
	log("Placing non-geolocated accounts")
	for _ in range(5):
		for pubkey in output["accounts"]:
			account = output["accounts"][pubkey]
			if account[2] == None and (account[1] == None or account[1][2] == None):
				pos = [0.0, 0.0]
				n_neighbors = 0
				for neighbor_pubkey in neighbors[pubkey]:
					neighbor = output["accounts"][neighbor_pubkey]
					neighbor_pos = neighbor[1][2] if neighbor[1] and neighbor[1][2] else (neighbor[2] if neighbor[2] else None)
					if neighbor_pos:
						pos[0] += neighbor_pos[0]
						pos[1] += neighbor_pos[1]
						n_neighbors += 1
				if n_neighbors != 0:
					account[2] = [pos[0]/n_neighbors, pos[1]/n_neighbors]
	
	if do_repulsion:
		log("Computing force layout")
		
		# Make temporary Mercator projection of size 360*360 to simplify layout calculations
		positions = []
		origs = []
		speeds = []
		for pubkey in output["accounts"]:
			account = output["accounts"][pubkey]
			pos = None
			if account[1] and account[1][2]:
				pos = account[1][2]
			elif account[2]:
				pos = account[2]
			else:
				continue
			if abs(pos[0]) >= MERCATOR_MAX_LAT:
				continue
			pos[0] = degrees(mercator(pos[0]))
			positions.append(pos)
			origs.append(pos.copy())
			speeds.append([0.0, 0.0])
		
		for i in range(LAYOUT_ITERATIONS):
			log("Force layout: iteration {}/{}".format(i, LAYOUT_ITERATIONS), LOG_TRACE)
			for n1 in range(len(positions)):
				orig = origs[n1]
				p1 = positions[n1]
				s1 = speeds[n1]
				
				# Gravity (kg*d)
				s1[0] += LAYOUT_GRAVITY * (orig[0]-p1[0])
				s1[1] += LAYOUT_GRAVITY * (orig[1]-p1[1])
				
				# Repulsion (kr/d)
				for n2 in range(n1+1, len(positions)):
					p2 = positions[n2]
					s2 = speeds[n2]
					
					dx = p2[0]-p1[0]
					dy = p2[1]-p1[1]
					d2 = dx**2 + dy**2
					if d2 < LAYOUT_MINDIST**2:
						if d2 < (LAYOUT_MINDIST/10)**2:
							d2 = (LAYOUT_MINDIST/10)**2
							if dx == 0:
								dx += random.uniform(-1,1)*LAYOUT_MINDIST/20
							if dy == 0:
								dy += random.uniform(-1,1)*LAYOUT_MINDIST/20
						d2 = max((LAYOUT_MINDIST/10)**2, d2) / LAYOUT_REPULSION
						dx = min(LAYOUT_MAXDIST, dx/d2)
						dy = min(LAYOUT_MAXDIST, dy/d2)
						
						s1[0] -= dx
						s1[1] -= dy
						s2[0] += dx
						s2[1] += dy
			
			# Apply forces
			for n1 in range(len(positions)):
				positions[n1][0] += speeds[n1][0]
				positions[n1][1] += speeds[n1][1]
				speeds[n1][0] = 0.0
				speeds[n1][1] = 0.0
		
		for pos in positions:
			pos[0] = mercator_recip(min(360,max(0,pos[0])))
			pos[1] = (pos[1]+180)%360-180
	
	log("Exporting data")
	f = open(output_dir+"mapdata.json", "w")
	json.dump(output, f, separators=(",",":"))
	f.close()
	
	log("Generating relation circles data")
	key_data = {}
	for pubkey in output["accounts"]:
		circles = getRelationCircles(pubkey, 2)
		key_data[pubkey] = {
			"account": output["accounts"][pubkey],
			"level1": [output["accounts"][key] for key in circles[1]],
			"level2": [output["accounts"][key] for key in circles[2]]
		}
        
	for pubkey, data in key_data.items():  
		with open(f"{output_dir}key_{pubkey}.json", "w") as f:
			json.dump(data, f, separators=(",", ":"))

	if do_overlays:
		log("Generating overlays")
		
		# Account density
		maxd = overlay_account_density[max(overlay_account_density, key=lambda p: overlay_account_density[p])]
		img = Image.new("RGBA", (overlay_size, overlay_size))
		for y in range(overlay_size):
			for x in range(overlay_size):
				img.putpixel((x,y), (255,0,0,32+int(overlay_account_density[(x,y)]/maxd*223)) if (x,y) in overlay_account_density else (0,0,0,0))
		img.save(output_dir+"overlay_account_density.png");
		
		# Money density
		maxd = overlay_money_density[max(overlay_money_density, key=lambda p: overlay_money_density[p])]
		img = Image.new("RGBA", (overlay_size, overlay_size))
		for y in range(overlay_size):
			for x in range(overlay_size):
				img.putpixel((x,y), (0,0,255,32+int(overlay_money_density[(x,y)]/maxd*223)) if (x,y) in overlay_money_density else (0,0,0,0))
		img.save(output_dir+"overlay_money_density.png");
		
		# Transaction density
		overlay_tx_density = {}
		block_archive = [int(i.split("_")[1].split("-")[0]) for i in os.listdir(duniter_path+"g1")]
		block_archive.sort(reverse=True)
		from_time = current_block["medianTime"] - 31557600
		loop = True
		for f in block_archive:
			if not loop:
				break
			f = open(duniter_path+"g1/chunk_{}-250.json".format(f), "r")
			blocks = json.load(f)
			for block in blocks["blocks"]:
				if block["medianTime"] < from_time:
					loop = False
					continue
				for tx in block["transactions"]:
					tx_result = get_tx_result(tx)
					for pubkey in tx_result:
						if pubkey in output["accounts"]:
							account = output["accounts"][pubkey]
							if account[1]:
								pos = (int((account[1][2][1]+180)/overlay_precision), int(mercator(account[1][2][0])/tau*overlay_size))
								
								if pos in overlay_tx_density:
									if tx_result[pubkey] > 0:
										overlay_tx_density[pos][0] += tx_result[pubkey]
									else:
										overlay_tx_density[pos][1] -= tx_result[pubkey]
								else:
									overlay_tx_density[pos] = [tx_result[pubkey],0] if tx_result[pubkey]>0 else [0,-tx_result[pubkey]]
							
							if tx_result[pubkey] > 0:
								moredata[pubkey][3] += tx_result[pubkey]
							else:
								moredata[pubkey][4] -= tx_result[pubkey]
			f.close()
		
		maxd = sum(overlay_tx_density[max(overlay_tx_density, key=lambda p: sum(overlay_tx_density[p]))])
		img = Image.new("RGBA", (overlay_size, overlay_size))
		for y in range(overlay_size):
			for x in range(overlay_size):
				if (x,y) in overlay_tx_density:
					v = overlay_tx_density[(x,y)]
					s = sum(v)
					r = 255 if s == 0 else int(v[0]/s*510)
					img.putpixel((x,y), (min(255,r),min(255,510-r),0,64+int(s/maxd*191)))
				else:
					img.putpixel((x,y), (0,0,0,0))
		img.save(output_dir+"overlay_tx_density.png");
	
	# Export CSV
	f = open(output_dir+"mapdata.csv", "w")
	f.write("Pubkey\tPseudo\tMember\tCommunity\tCerts received\tCerts issued\tBalance\tAmount received\tAmount sent\tTitle\tLatitude\tLongitude\n")
	for pubkey in output["accounts"]:
		account = output["accounts"][pubkey]
		mdata = moredata[pubkey]
		f.write("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(
			pubkey,
			*(account[0] if account[0] else ("","","")),
			mdata[0], mdata[1],
			mdata[2],
			mdata[3], mdata[4],
			account[1][0] if account[1] else "",
			*(mdata[5] if mdata[5] else ("","")),
		))
	f.close()


"""
pubkey => [[pseudo:str, member:bool, community:uint]?, [title:str?, avatar:bool, [lat:float, lon:float]?]?, [computed_lat:float, computed_lon:float]?]
"""
