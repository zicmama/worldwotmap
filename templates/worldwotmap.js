/*
    CopyLeft 2018-2022 Pascal Engélibert & Fred
    This file is part of WorldWotMap.

    WorldWotMap is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    WorldWotMap is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with WorldWotMap.  If not, see <https://www.gnu.org/licenses/>.
*/

// Instance settings
const SETTINGS = {
    "cesiumplus_url": "https://g1.data.presles.fr/",
    "cesium_url": "https://ipfs.copylaradio.com/ipfs/QmXex8PTnQehx4dELrDYuZ2t5ag85crYCBxm3fcTjVWo2k",
    "data_url": "{{base-path}}data/"
};

const MAP_TILES = {
    "classic": ["{{tiles-classic}}", "https://{s}.tile.osm.org/{z}/{x}/{y}.png", '{{tiles-classic-attribution}}'],
    "toner-lite": ["{{tiles-toner-lite}}", "https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}.png", '{{tiles-toner-lite-attribution}}']
};
const MAP_DEFAULT_TILES = "classic";
var map_default_pos = [46.725, 2.834];
var map_default_zoom = 6;
var COMMUNITY_COLORS = ["#ff0000", "#00ff00", "#0000ff", "#c0c000", "#c000c0", "#00c0c0", "#d05000", "#d00050", "#50d000", "#00d050", "#d00050", "#00d050"];
var breadcrumb = [];

class Account {
    constructor(pubkey, member=false, uid=null, title=null, pos=null, point=null, avatar=false, community=null) {
        this.pubkey = pubkey;
        this.member = member;
        this.uid = uid
        this.title = title;
        this.pos = pos;
        this.point = point;
        this.certsIssued = [];
        this.certsReceived = [];
        this.avatar = avatar;
        this.community = community;
        this.txPubkeys = [];
    }

    mapActiveAccount() {// `this` is the Leaflet Circle, not the Account
        mapActiveAccount(this.options["pubkey"]);
    }
}

class Cert {
    constructor(issuer, receiver, line) {
        this.issuer = issuer;
        this.receiver = receiver;
        this.line = line;
    }
}

class Community {
    constructor(color) {
        this.color = color;
    }
}

function updateRelationCircles(pubkey, depth) {
    let circles = getRelationCircles(pubkey, depth);
    // Effacer les anciens polygones
    l_relationCircles.clearLayers();
    $("#circle-searchresults").empty();

    // Dessiner les polygones pour chaque niveau
    for (let level = 1; level < circles.length; level++) {
        let keys = circles[level];
        let points = keys.map(k => accounts[k].pos).filter(p => p !== null);

        if (points.length > 2) {
            // Créer un polygone concave à partir des points
            let hull = d3.polygonHull(points);

            if (hull) {
                let polygon = L.polygon(hull, {
                    color: `hsl(${level*60}, 100%, 50%)`,
                    fillOpacity: 0.2
                }).addTo(l_relationCircles);
            }
            // Ajouter la liste des comptes pour ce niveau
            let accountList = $("<details>").addClass("searchresult").appendTo("#circle-searchresults");
            $("<summary>").html(`N${level} <span>(${keys.length} comptes)</span>`).appendTo(accountList);

            let accountListItems = $("<ul>").appendTo(accountList);

            keys.forEach(pubkey => {
                let account = accounts[pubkey];
                let listItem = $("<li>").appendTo(accountListItems);
                $("<a>").attr("href", "#").text(account.title || pubkey.substr(0, 8)).click(() => {
                    search(pubkey, true, true);
                }).appendTo(listItem);

                // Ajouter le lien vers Cesium
                $("<a>")
                    .attr("href", settings["cesium_url"] + "#/app/wot/" + pubkey + "/")
                    .attr("target", "_blank")
                    .html('&nbsp;&nbsp;<img width=20 src="https://ipfs.copylaradio.com/ipfs/QmQd7rNi13eHHWaEfqKLHxmY6V5HWakGsXNZH8QVoGQvNt" alt="Cs" title="Voir sur Cesium" style="height: 16px; vertical-align: middle;">')
                    .appendTo(listItem);

                // Lien vers UPlanet
                $("<a>")
                    .attr("href", "https://qo-op.com")
                    .attr("target", "_blank")
                    .html('&nbsp;&nbsp;<img width=16px src="https://ipfs.copylaradio.com/ipfs/QmaLzMFUR7QfKvxp63KkHkPco92maRSiNvPEgMoKNxM7Q8"  alt="TW" title="TW ?" style="height: 16px; vertical-align: middle;">')
                    .appendTo(listItem);
            });
        }
    }
}

function zoomToSelectedAccount() {
    if (active_account) {
        map.setView(accounts[active_account].pos, 13);
    } else {
        map.locate({ setView: true, maxZoom: 13, watch: true });
    }
    showURL();
}

function resetBreadcrumb() {
  breadcrumb = [];
  mapInactiveAccount(active_account);
  updateBreadcrumb();
  // Positionne la carte sur la position actuelle de l'utilisateur
  map.locate({ setView: true, maxZoom: 13, watch: true });
}

function updateBreadcrumb() {
  var breadcrumbHtml = '';
  breadcrumb.forEach(function(pubkey, index) {
    var account = accounts[pubkey];
    breadcrumbHtml += '<a href="#" onclick="goToBreadcrumb(' + index + '); return false;">' + (account.title || pubkey.substr(0, 8)) + '</a>';
    if (index < breadcrumb.length - 1) {
      breadcrumbHtml += ' &raquo; ';
    }
  });
  $('#breadcrumb').html(breadcrumbHtml);
}

function goToBreadcrumb(index) {
  breadcrumb = breadcrumb.slice(0, index + 1);
  mapActiveAccount(breadcrumb[breadcrumb.length - 1]);
}

function mapActiveAccount(pubkey) {
    var account = accounts[pubkey];
    console.log(account);
    if(active_account != null && active_account != account.pubkey)
        mapInactiveAccount(active_account);
    active_account = account.pubkey;
    onlyShowCertsOfSelected();

    $("#mapmenu-idcard").attr("style", "display: initial;");
    $("#mapmenu-idcard-bt").attr("onclick", "mapInactiveAccount('"+active_account+"');");
    $("#mapmenu-idcard .idcard-title").html(account.title !== null ? account.title : "");
    $("#mapmenu-idcard .idcard-pubkey").html(active_account);
    $("#mapmenu-idcard .idcard-pubkey-short").html(active_account.substr(0,8));
    if(account.avatar) {
        $("#mapmenu-idcard .idcard-avatar").attr("src", settings["cesiumplus_url"]+"user/profile/"+active_account+"/_image/avatar.png");
        $("#mapmenu-idcard .idcard-avatar").attr("style", "display: initial;");
    } else {
        $("#mapmenu-idcard .idcard-avatar").attr("src", "");
        $("#mapmenu-idcard .idcard-avatar").attr("style", "display: none;");
    }
    if(account.community != null) {
        $("#mapmenu-idcard .idcard-community").html("{{community-number}} <strong>"+account.community+"</strong>");
        $("#mapmenu-idcard .idcard-community").attr("style", "border-left:1em solid "+communities[account.community].color+";");
    } else {
        $("#mapmenu-idcard .idcard-community").html("");
        $("#mapmenu-idcard .idcard-community").attr("style", "");
    }
    $("#mapmenu-idcard .idcard-ncerts-received").html(account.certsReceived.length);
    $("#mapmenu-idcard .idcard-ncerts-issued").html(account.certsIssued.length);
    $("#mapmenu-idcard-cesiumlink").attr("href", settings["cesium_url"]+"#/app/wot/"+active_account+"/");
    for(var cert in account.certsIssued) {
        if(line = certs[account.certsIssued[cert]].line)
            line.setStyle({color:"#4f4", weight:3, opacity:0.6});
    }
    for(var cert in account.certsReceived) {
        if(line = certs[account.certsReceived[cert]].line)
            line.setStyle({color:"#f44", weight:3, opacity:0.6, dashArray: '30,30'});
    }

    showURL();

    let depth = parseInt(document.getElementById("mapmenu-idcard-onlycertsofsel-n").value);
    updateRelationCircles(pubkey, depth);

    var geoIcon = L.icon({
        iconUrl: 'marker-icon-2x.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
    });
    var userIcon = L.icon({
        iconUrl: 'marker-icon-2x-green.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
    });

    // Remove old markers
    map.eachLayer(function(layer) {
        if (layer instanceof L.Marker) {
            layer.remove();
        }
    });

    var account = accounts[pubkey];
    // Afficher le compte actif avec une icône verte
    L.marker(account.pos, { icon: userIcon }).addTo(map)
    .bindPopup("<b>" + account.title + "</b><br/>" + account.pubkey)
    .openPopup();

    // Calculer les points environnants
    var surroundingPoints = [];
    // Points avec une précision de 1°
    surroundingPoints.push({ lat: Math.floor(account.pos[0]), lng: Math.floor(account.pos[1]) }); // Coin inférieur gauche
    surroundingPoints.push({ lat: Math.floor(account.pos[0]), lng: Math.floor(account.pos[1]) + 1 }); // Coin inférieur droit
    surroundingPoints.push({ lat: Math.floor(account.pos[0]) + 1, lng: Math.floor(account.pos[1]) }); // Coin supérieur gauche
    surroundingPoints.push({ lat: Math.floor(account.pos[0]) + 1, lng: Math.floor(account.pos[1]) + 1 }); // Coin supérieur droit

    // Points avec une précision de 0.1°
    surroundingPoints.push({ lat: Math.floor(account.pos[0] * 10) / 10, lng: Math.floor(account.pos[1] * 10) / 10 }); // Coin inférieur gauche
    surroundingPoints.push({ lat: Math.floor(account.pos[0] * 10) / 10, lng: (Math.floor(account.pos[1] * 10) / 10) + 0.1 }); // Coin inférieur droit
    surroundingPoints.push({ lat: (Math.floor(account.pos[0] * 10) / 10) + 0.1, lng: Math.floor(account.pos[1] * 10) / 10 }); // Coin supérieur gauche
    surroundingPoints.push({ lat: (Math.floor(account.pos[0] * 10) / 10) + 0.1, lng: (Math.floor(account.pos[1] * 10) / 10) + 0.1 }); // Coin supérieur droit

    // Points avec une précision de 0.01°
    surroundingPoints.push({ lat: Math.floor(account.pos[0] * 100) / 100, lng: Math.floor(account.pos[1] * 100) / 100 }); // Coin inférieur gauche
    surroundingPoints.push({ lat: Math.floor(account.pos[0] * 100) / 100, lng: (Math.floor(account.pos[1] * 100) / 100) + 0.01 }); // Coin inférieur droit
    surroundingPoints.push({ lat: (Math.floor(account.pos[0] * 100) / 100) + 0.01, lng: Math.floor(account.pos[1] * 100) / 100 }); // Coin supérieur gauche
    surroundingPoints.push({ lat: (Math.floor(account.pos[0] * 100) / 100) + 0.01, lng: (Math.floor(account.pos[1] * 100) / 100) + 0.01 }); // Coin supérieur droit

    // Ajouter des marqueurs pour les points environnants
    surroundingPoints.forEach(point => {
        var marker = L.marker([point.lat, point.lng], { icon: geoIcon }).addTo(map);
        var distance = L.latLng(account.pos).distanceTo([point.lat, point.lng]);
        marker.bindPopup("<h2><a href='https://www.openstreetmap.org/directions?engine=fossgis_osrm_foot&route=" + account.pos[0] + "%2C" + account.pos[1] + "%3B" + point.lat + "%2C" + point.lng + "' target='_blank'>GOTO</a></h2>" +
        "Geo Key : _" + point.lat.toFixed(2) + "_" + point.lng.toFixed(2) +
        "<br>Distance : " + distance.toFixed(2) + " m" +
        "<h3><a target='_blank' href='https://astroport.copylaradio.com/?uplanet=@&zlat=" + point.lat.toFixed(2) + "&zlon=" + point.lng.toFixed(2) + "'>UPlanet</a></h3>"
        );
    });
    if (!breadcrumb.includes(pubkey)) {
    breadcrumb.push(pubkey);
    }
    updateBreadcrumb();

}

function mapInactiveAccount(pubkey) {
    if(accounts[active_account].point.isPopupOpen())
        accounts[active_account].point.closePopup();
    active_account = null;
    onlyShowCertsOfSelected()
    var account = accounts[pubkey];
    $("#mapmenu-idcard").attr("style", "display: none;");
    $("#mapmenu-idcard").attr("onclick", "");
    $("#mapmenu-idcard .idcard-title").html("");
    $("#mapmenu-idcard .idcard-pubkey").html("");
    $("#mapmenu-idcard .idcard-pubkey-short").html();
    $("#mapmenu-idcard .idcard-avatar").attr("style", "display: none;");
    $("#mapmenu-idcard .idcard-community").html("");
    $("#mapmenu-idcard .idcard-community").attr("style", "");
    $("#mapmenu-idcard-cesiumlink").attr("href", "#");
    $("#mapmenu-idcard-goto").attr("onclick", "zoomToSelectedAccount();");
    for(var cert in account.certsIssued) {
        certs[account.certsIssued[cert]].line.setStyle({color:"black", weight:1, opacity:0.2});
    }
    for(var cert in account.certsReceived) {
        certs[account.certsReceived[cert]].line.setStyle({color:"black", weight:1, opacity:0.2});
    }
    onlyShowCertsOfSelected();
    showURL();
}

// mode: true=received / false=issued
function getAccountsWithDistance(pubkey, distance, mode, tx=false) {
    var c = [];// certs
    var a = {};// accounts
    a[pubkey] = 0;

    var d = 0;
    var as = {0:pubkey};// accounts stack
    var cs = {0:[]};// certs stack

    if(mode)
        cs[0] = accounts[pubkey].certsReceived.slice();
    else
        cs[0] = accounts[pubkey].certsIssued.slice();

    var i = 0;
    while(cs[0].length > 0 && i < 10000) {
        while(!(d in cs) || cs[d].length == 0 || d >= distance) {
            d --;
        }
        var cert = cs[d].pop();
        d ++;
        c.push(cert);
        if(mode) {
          if(!(certs[cert].issuer in a) || d < a[certs[cert].issuer]) {
            a[certs[cert].issuer] = d;
            as[d] = certs[cert].issuer;
            cs[d] = accounts[certs[cert].issuer].certsReceived.slice();
          }
        }
        else {
            if(!(certs[cert].receiver in a) || d < a[certs[cert].receiver]) {
                a[certs[cert].receiver] = d;
                as[d] = certs[cert].receiver;
                cs[d] = accounts[certs[cert].receiver].certsIssued.slice();
            }
        }
        i ++;
    }
    return [a, c];
}

function onlyShowCertsOfSelected() {
    for(cert in active_certs) {
        active_certs[cert][1].remove();
    }
    active_certs = [];
    if(active_account == null || !document.getElementById("mapmenu-idcard-onlycertsofsel").checked) {
        // Réafficher les calques par défaut si aucun compte n'est sélectionné ou si la case n'est pas cochée
        p_certlines.style.display = "initial";
        l_accounts_cert.addTo(map);
        l_accounts_wallet.addTo(map);
        return;
      }

      // Cacher les calques des comptes
      l_accounts_cert.remove();
      l_accounts_wallet.remove();
      p_certlines.style.display = "none";

      let depth = parseInt(document.getElementById("mapmenu-idcard-onlycertsofsel-n").value);

      if (displayMode === "circles") {

        updateRelationCircles(active_account, depth);

        // Afficher le calque des cercles de relation
        if (!map.hasLayer(l_relationCircles)) {
          l_relationCircles.addTo(map);
        }
    } else {
        // Cacher la couche des cercles de relation
        if (map.hasLayer(l_relationCircles)) {
            l_relationCircles.removeFrom(map);
        }
        console.log("Display mode: lines");
        var show_certs = [];
        if(document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "all" ||
        document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "received") {
            show_certs = show_certs.concat(getAccountsWithDistance(active_account, document.getElementById("mapmenu-idcard-onlycertsofsel-n").value, true)[1]);
        }
        if(document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "all" ||
        document.getElementById("mapmenu-idcard-onlycertsofsel-t").value == "issued") {
            show_certs = show_certs.concat(getAccountsWithDistance(active_account, document.getElementById("mapmenu-idcard-onlycertsofsel-n").value, false)[1]);
        }
        for(var cert in show_certs) {
            if(accounts[certs[show_certs[cert]].issuer].pos !== null && accounts[certs[show_certs[cert]].receiver].pos !== null) {
                var color = "black";
                if(certs[show_certs[cert]].issuer == active_account)
                    color = "#4f4";
                else
                    color = "#f44";
                var line = L.polyline([accounts[certs[show_certs[cert]].issuer].pos, accounts[certs[show_certs[cert]].receiver].pos], {color:color, weight:2, opacity:0.7}).addTo(map);
                line.bindPopup("<b>"+accounts[certs[show_certs[cert]].issuer].title+"</b> &rarr; <b>"+accounts[certs[show_certs[cert]].receiver].title+"</b>");
                line.addTo(l_certlines);
                active_certs.push([show_certs[cert], line]);
                active_ncerts ++;
            }
        }
    }
}

function showURL() {
    var pos = map.getCenter();
    var url = window.location.pathname + "?lat="+pos.lat+"&lon="+pos.lng+"&zoom="+map.getZoom();
    var selected_overlays = (map.hasLayer(l_accounts_cert)
        | map.hasLayer(l_accounts_wallet) <<1
        | map.hasLayer(l_certlines) <<2
        | map.hasLayer(l_communities) <<3
        | map.hasLayer(l_account_density) <<4
        | map.hasLayer(l_money_density) <<5
        | map.hasLayer(l_tx_density) <<6
        | map.hasLayer(l_accounts_autopos) <<7);
    if(selected_overlays != 7)
        url += "&ol="+selected_overlays;
    if(active_account != null)
        url += "&a="+active_account;
    if(document.getElementById("mapmenu-idcard-onlycertsofsel").checked)
        url += "&only="+document.getElementById("mapmenu-idcard-onlycertsofsel-n").value;
    for(tile in MAP_TILES) {
        if(tile_layers[MAP_TILES[tile][0]]._leaflet_id in map._layers) {
            if(tile == MAP_DEFAULT_TILES)
                break;
            url += "&bl="+tile;
        }
    }
    $("#mapmenu-maplink").attr("href", url+"#map");
}

function search(term, goto=true, popup=true, title=true) {
    term = term.trim();
    var obj = document.getElementById("mapmenu-searchresults");
    obj.innerHTML = "";
    if(term == "")
        return 0;
    if(term in accounts) {// lazy
        mapActiveAccount(term);
        map.setView(accounts[active_account].pos);
        if(popup)
            accounts[active_account].point.openPopup();
        else
            accounts[active_account].point.fire("popupopen");
        alertOnMap(accounts[active_account].pos);
        return 1;
    }
    // zealous
    var results = [];
    if(term.match(/^[0-9a-z]{1,43}$/i)) {
        var re = new RegExp("^"+term, "i");// starting by
        for(a in accounts) {
            if(accounts[a].pos && a.match(re)) {
                results.push(a);
                if(results.length > 20)// too many results
                    break;
            }
        }
    }
    if(title) {
        var re = new RegExp(term, "i");
        for(a in accounts) {
            if(accounts[a].pos
                && (
                    (accounts[a].title !== null && accounts[a].title.match(re))
                    || (accounts[a].uid !== null && accounts[a].uid.match(re))
                )
                && results.indexOf(a) == -1)
            {
                results.push(a);
                if(results.length > 20)// too many results
                    break;
            }
        }
    }
    if(results.length == 1 && goto) {
        mapActiveAccount(results[0]);
        map.setView(accounts[active_account].pos);
        if(popup)
            accounts[active_account].point.openPopup();
        else
            accounts[active_account].point.fire("popupopen");
        alertOnMap(accounts[active_account].pos);
    }
    else
        showResults(obj, results, popup);
}

function getRelationCircles(pubkey, maxLevel) {
    let circles = [];

    // Niveau 0 = la clé elle-même
    circles[0] = [pubkey];

    for (let level=1; level<=maxLevel; level++) {
      circles[level] = [];

      // Parcourir les clés du niveau précédent
      for (let key of circles[level-1]) {
        let account = accounts[key];

        // Ajouter les destinataires des certifications émises
        for (let cert of account.certsIssued) {
          if (!circles[level].includes(certs[cert].receiver)) {
            circles[level].push(certs[cert].receiver);
          }
        }

        // Ajouter les émetteurs des certifications reçues
        for (let cert of account.certsReceived) {
          if (!circles[level].includes(certs[cert].issuer)) {
            circles[level].push(certs[cert].issuer);
          }
        }
      }
    }

    return circles;
}

function toggleDisplayMode() {
    if (displayMode === "lines") {
      displayMode = "circles";
      document.getElementById("mapmenu-idcard-displaymode").textContent = "{{button-show-lines}}";
    } else {
      displayMode = "lines";
      document.getElementById("mapmenu-idcard-displaymode").textContent = "{{button-show-circles}}";
    }
    onlyShowCertsOfSelected();
  }

// algorithme de mélange de Fisher-Yates
function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

function geosearch(goto=true, popup=true) {
    var bounds = map.getBounds();
    var obj = document.getElementById("mapmenu-searchresults");
    obj.innerHTML = "";
    var results = [];
    for (var a in accounts) {
        var account = accounts[a];
        if (account.pos
            && account.pos[0] < bounds._northEast.lat
            && account.pos[1] < bounds._northEast.lng
            && account.pos[0] > bounds._southWest.lat
            && account.pos[1] > bounds._southWest.lng) {
            results.push(a);
        }
    }

    // Mélanger les résultats
    results = shuffle(results);

    // Limiter les résultats à 20 comptes
    results = results.slice(0, 20);

    if (results.length == 1 && goto) {
        mapActiveAccount(results[0]);
        map.setView(accounts[active_account].pos);
        if (popup)
            accounts[active_account].point.openPopup();
        else
            accounts[active_account].point.fire("popupopen");
        alertOnMap(accounts[active_account].pos);
    } else {
        showResults(obj, results, popup);
    }
}

function showResults(obj, results, popup=true) {
    var htmlresults = "";
    for(result in results) {
        var account = accounts[results[result]];
        htmlresults += '<div class="searchresult" onclick="search(\''+account.pubkey+'\','+popup+');" title="'+account.pubkey+(account.member?" / "+account.uid:"")+'">';
        if(!account.title && !account.member)
            htmlresults += '<code>'+account.pubkey.substr(0,8)+'</code>';
        else
            htmlresults += (account.title?account.title:"")+" "+(account.member?'<span class="membername">'+account.uid+'</span>':"");
        htmlresults += '</div>';
    }
    obj.innerHTML = htmlresults;
}

function alertOnMap(pos, radius=64, color="red", opacity=0.3) {
    var point = L.circle(pos, {
        color: color,
        fillColor: color,
        fillOpacity: opacity,
        radius: pixelsToMeters(pos[0], radius, map.getZoom())
    }).addTo(map);
    setTimeout(function(){point.removeFrom(map);}, 500);
}

function toggleDisplayMode() {
    if (displayMode === "lines") {
      displayMode = "circles";
      document.getElementById("mapmenu-idcard-displaymode").textContent = "{{button-show-lines}}";
    } else {
      displayMode = "lines";
      document.getElementById("mapmenu-idcard-displaymode").textContent = "{{button-show-circles}}";
    }
    onlyShowCertsOfSelected();
  }

document.getElementById("loading").style = "";

var settings = {};
Object.assign(settings, SETTINGS);
loadSettings();

var displayMode = "lines"; // "lines" or "circles"
var communities = [];
var ids = {};
var accounts = {};
var certs = [];
var naccounts = 0;
var nmembers = 0;
var nmembers_total = 0;
var ncerts = 0;
var active_certs = [];
var active_ncerts = [];
var active_account = null;
var map = L.map("map").setView([getParam("lat",map_default_pos[0]), getParam("lon",map_default_pos[1])], getParam("zoom",map_default_zoom));
var tile_layers = {};
for(i in MAP_TILES) {
    tile_layers[MAP_TILES[i][0]] = L.tileLayer(MAP_TILES[i][1], {attribution: MAP_TILES[i][2]});
}
var p_certlines = map.createPane("certlines");
var l_accounts_cert = L.layerGroup();
var l_accounts_autopos = L.layerGroup();
var l_accounts_wallet = L.layerGroup();
var l_certlines = L.layerGroup([]);
var l_communities = L.layerGroup([]);
var l_relationCircles = L.layerGroup([]);
var l_account_density = L.imageOverlay(settings["data_url"]+"overlay_account_density.png", [[90,-180],[-90,180]]);
var l_money_density = L.imageOverlay(settings["data_url"]+"overlay_money_density.png", [[90,-180],[-90,180]]);
var l_tx_density = L.imageOverlay(settings["data_url"]+"overlay_tx_density.png", [[90,-180],[-90,180]]);
var overlays = L.control.layers(tile_layers, {
    "{{layer-identities}}": l_accounts_cert,
    "{{layer-autopos-identities}}": l_accounts_autopos,
    "{{layer-wallets}}": l_accounts_wallet,
    "{{layer-certs}}": l_certlines,
    "{{layer-communities}}": l_communities,
    "{{layer-relation-circles}}": l_relationCircles,
    "{{layer-account-density}}": l_account_density,
    "{{layer-money-density}}": l_money_density,
    "{{layer-tx-density}}": l_tx_density
});
overlays.addTo(map);
tile_layers[MAP_TILES[getParam("bl", MAP_DEFAULT_TILES)][0]].addTo(map);
L.control.scale().addTo(map);

map.on("moveend", showURL);
map.on("zoomend", showURL);
map.on("layeradd", showURL);
map.on("layerremove", showURL);
map.on("baselayerchange", showURL);

$.get(settings["data_url"]+"mapdata.json", null, function(data) {
    document.getElementById("data-time").innerHTML = new Date(data["time"]*1000).toLocaleString();
    document.getElementById("currentblock-number").innerHTML = data["current_block"]["number"];
    document.getElementById("currentblock-hash").innerHTML = data["current_block"]["hash"];
    document.getElementById("currentblock-mediantime").innerHTML = new Date(data["current_block"]["median_time"]*1000).toLocaleString();

    // Communities
    $("#ncommunities").html(data["communities"] + " {{after-number-communities}}");
    var i = 0;
    while(i < data["communities"]) {
        communities.push(new Community(COMMUNITY_COLORS[i % COMMUNITY_COLORS.length]));
        i ++;
    }

    // Accounts
    var community_point_pks = [];
    for(pubkey in data["accounts"]) {
        var account_data = data["accounts"][pubkey];
        var account = new Account(pubkey);
        accounts[pubkey] = account;

        if(account_data[0]) {
            account.member = account_data[0][1];
            account.uid = account_data[0][0];
            account.community = account_data[0][2];

            if(account.member)
                nmembers_total ++;
        }

        if(account_data[1]) {
            account.title = account_data[1][0];
            account.avatar = account_data[1][1];

            if(account_data[1][2]) {
                account.pos = account_data[1][2];

                var point = L.circle(account.pos, {
                    color: account.member ? "#f0f" : "#ed9a00",
                    fillColor: account.member ? "#f0f" : "#ed9a00",
                    fillOpacity: 0.1,
                    radius: 50,
                    pubkey: pubkey
                }).addTo(map);
                point.bindPopup("<b>"+account.title+"</b><br/>"+pubkey);
                account.point = point;
                point.on("popupopen", account.mapActiveAccount);
                if(account.member)
                    point.addTo(l_accounts_cert);
                else
                    point.addTo(l_accounts_wallet);

                if(account.community !== null) {
                    var point = L.circle(account.pos, {
                        color: communities[account.community].color,
                        fillColor: communities[account.community].color,
                        fillOpacity: 0.2,
                        radius: 100,
                        pubkey: pubkey
                    }).addTo(map);
                    point.addTo(l_communities);
                    community_point_pks.push(pubkey);
                }

                if(account.member)
                    nmembers ++;
                naccounts ++;
            }
        }

        if(account_data[2]) {
            account.pos = account_data[2];

            var point = L.circle(account.pos, {
                color: account.member ? "#f0f" : "#ed9a00",
                fillColor: account.member ? "#f0f" : "#ed9a00",
                fillOpacity: 0.1,
                radius: 50,
                pubkey: pubkey
            }).addTo(map);
            account.point = point;
            point.on("popupopen", account.mapActiveAccount);
            point.addTo(l_accounts_autopos);

            if(account.community !== null) {
                var point = L.circle(account.pos, {
                    color: communities[account.community].color,
                    fillColor: communities[account.community].color,
                    fillOpacity: 0.2,
                    radius: 100,
                    pubkey: pubkey
                }).addTo(map);
                point.addTo(l_communities);
                community_point_pks.push(pubkey);
            }

            if(account.member)
                nmembers ++;
            naccounts ++;
        }
    }

    document.getElementById("naccounts").innerHTML = naccounts + " {{after-number-accounts}}";
    document.getElementById("nmembers").innerHTML = nmembers + " / " + nmembers_total + " {{after-number-members}}";
    l_accounts_wallet.addTo(map);
    l_accounts_cert.addTo(map);
    l_accounts_autopos.addTo(map);
    l_accounts_autopos.remove(map);
    l_communities.addTo(map);
    l_communities.remove(map);

    // Certifications
    for(i in data["certs"]) {
        var cert = data["certs"][i];
        var issuer = accounts[cert[0]];
        var recipient = accounts[cert[1]];

        if(issuer.pos !== null && recipient.pos !== null) {
            var line = L.polyline([issuer.pos, recipient.pos], {color:'black', weight:1, opacity:0.2, pane:"certlines"}).addTo(map);
            line.bindPopup("<b>"+issuer.uid+"</b> &rarr; <b>"+recipient.uid+"</b><br />"+round(geoDist(issuer.pos, recipient.pos),2)+" km");
            line.addTo(l_certlines);
            ncerts ++;
        }

        certs.push(new Cert(cert[0], cert[1], line));
        issuer.certsIssued.push(i);
        recipient.certsReceived.push(i);
    }

    document.getElementById("ncerts").innerHTML = ncerts + " / " + data["certs"].length + " {{after-number-certs}}";
    l_certlines.addTo(map);
    l_certlines.remove(map);

    // Referees
    for(i in accounts) {
        if(accounts[i].point !== null && accounts[i].certsIssued.length >= 5 && accounts[i].certsReceived.length >= 5) {
            accounts[i].point.setStyle({color: "#0c0", fillColor: "#0c0"});
        }
    }

    // Select account given in URL
    var url_account = getParam("a");
    if(url_account != null) {
        if(search(url_account, true, getParam("popup")) == 1) {
            if(getParam("only", 0) > 0) {
                $("#idcard-onlycertsofsel").attr("checked", "checked");
                $("#idcard-onlycertsofsel-n").attr("value", getParam("only"));
                onlyShowCertsOfSelected();
            }
        }
    }

    // Select layers given in URL
    var selected_overlays = getParam("ol");
    if(selected_overlays !== null) {
        selected_overlays = parseInt(selected_overlays);
        if(!(selected_overlays & 1)) l_accounts_cert.remove(map);
        if(!(selected_overlays & 2)) l_accounts_wallet.remove(map);
        if(selected_overlays & 4) l_certlines.addTo(map);
        if(selected_overlays & 8) l_communities.addTo(map);
        if(selected_overlays & 16) l_account_density.addTo(map);
        if(selected_overlays & 32) l_money_density.addTo(map);
        if(selected_overlays & 64) l_tx_density.addTo(map);
        if(selected_overlays & 128) l_accounts_autopos.addTo(map);
    }

    showURL();

    // Voronoi communities
    var voronoi = d3.voronoi()
      .x(function(a) { return accounts[a].pos[0]; })
      .y(function(a) { return accounts[a].pos[1]; });

    var _polygons = voronoi(community_point_pks).polygons();
    for(_poly in _polygons) {
        if(_polygons[_poly].indexOf(null) == -1)
            L.polygon(_polygons[_poly], {stroke: false, fillOpacity: 0.3, color: communities[accounts[_polygons[_poly].data].community].color}).addTo(l_communities);
    }

    document.getElementById("loading").style = "display:none;";
});
